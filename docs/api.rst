API reference
==================

Main module: smallerize
-----------------------------------

.. automodule:: smallerize.smallerize
    :members:
    :undoc-members:

simulate
--------------------------

.. automodule:: smallerize.simulate
    :members:
    :undoc-members:


