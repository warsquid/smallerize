.. include:: ../README.rst

Detailed documentation
----------------------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   usage
   trial_setup
   minimization
   api
   authors
   history

Indices and tables
-------------------
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
