# -*- coding: utf-8 -*-

"""Top-level package for smallerize."""

__author__ = """Marius Mather"""
__email__ = 'marius.mather@gmail.com'
__version__ = '0.5.0'

# __all__ is defined so this will only import the
# desired objects
from .smallerize import Factor, Arm, Minimizer

__all__ = ['Factor', 'Arm', 'Minimizer']
