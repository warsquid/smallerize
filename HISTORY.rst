=======
History
=======

0.5.0 (2019-05-08)
-------------------

* Improved performance with large numbers of factors/factor levels by
  making the count table sparse.

0.4.0 (2019-02-02)
------------------

* Switched to Mozilla Public License.

0.3.0 (2019-01-02)
------------------

* Make it easier to check the valid total imbalance and
  probability methods.

0.2.0 (2018-10-22)
------------------

* Add the biased coin minimization method to allow
  for unequal treatment allocations.

0.1.0 (2018-10-07)
------------------

* First release on PyPI.
